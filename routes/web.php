<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\EventController;
use App\Http\Middleware\RespondWithYaml;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::post('webhook', [EventController::class, 'handler'])->middleware(RespondWithYaml::class);

Route::middleware('stateful')->group(function () {
    Route::get('/', [HomeController::class, 'view']);

    Route::get('instance/new')->name('instance.new');
    Route::get('project/new')->name('project.new');
});

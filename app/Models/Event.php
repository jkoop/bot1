<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model {
    protected $casts = [
        'data' => 'json',
    ];

    protected $fillable = [
        'uuid',
        'data',
    ];

    protected $primaryKey = 'uuid';
}

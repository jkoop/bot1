<?php

namespace App\Exceptions;

use Exception;

class UnrecognizedInstanceException extends Exception {
    protected $message = "Unrecognized 'X-Gitlab-Instance' request header.";

    public function __construct(
        readonly string $instanceUrl,
    ) {
    }

    public function render() {
        return response()->json([
            'message' => $this->message,
            'hint' => route('instance.new', [
                'instance_url' => $this->instanceUrl,
            ]),
        ], 400);
    }
}

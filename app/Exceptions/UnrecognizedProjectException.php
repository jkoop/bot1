<?php

namespace App\Exceptions;

use Exception;

class UnrecognizedProjectException extends Exception {
    protected $message = "Unrecognized project ID.";

    public function __construct(
        readonly string $instanceUrl,
        readonly int $projectId,
    ) {
    }

    public function render() {
        return response()->json([
            'message' => $this->message,
            'hint' => route('project.new', [
                'instance_url' => $this->instanceUrl,
                'project_id' => $this->projectId,
            ]),
        ], 400);
    }
}

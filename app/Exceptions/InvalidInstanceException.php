<?php

namespace App\Exceptions;

use Exception;

class InvalidInstanceException extends Exception {
    protected $message = "Invalid 'X-Gitlab-Instance' request header.";

    public function render() {
        return response()->json([
            'message' => $this->message,
        ], 400);
    }
}

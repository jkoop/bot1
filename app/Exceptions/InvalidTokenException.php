<?php

namespace App\Exceptions;

use Exception;

class InvalidTokenException extends Exception {
    protected $message = "Invalid 'X-Gitlab-Token' request header.";

    public function render() {
        return response()->json([
            'message' => $this->message,
        ], 401);
    }
}

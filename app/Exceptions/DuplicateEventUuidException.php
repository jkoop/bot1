<?php

namespace App\Exceptions;

use Exception;

class DuplicateEventUuidException extends Exception {
    protected $message = "An event with this UUID has already been received.";

    public function render() {
        return response()->json([
            'message' => $this->message,
        ], 425);
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RespondWithYaml {
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response {
        /**
         * OK, here's the plan:
         * 1. We tell Laravel that the client accepts JSON
         * 2. We convert the JSON response into YAML
         */

        // set the request Accept to JSON
        $request->headers->set('Accept', 'application/json');

        $response = $next($request);

        // convert JSON response to YAML
        if (str_starts_with($response->headers->get('Content-Type'), 'application/json')) {
            $response->headers->set('Content-Type', 'application/yaml');
            $response->setContent(yaml_emit(json_decode($response->getContent(), true)));
        }

        return $response;
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;

class HomeController extends Controller {
    public function view(): View {
        /**
         * some sort of statistics generating thingy would go here
         */

        return view('home');
    }
}

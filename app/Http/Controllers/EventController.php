<?php

namespace App\Http\Controllers;

use App\Exceptions\DuplicateEventUuidException;
use App\Exceptions\InvalidInstanceException;
use App\Exceptions\InvalidTokenException;
use App\Exceptions\UnrecognizedInstanceException;
use App\Exceptions\UnrecognizedProjectException;
use App\Jobs\ProcessEvent;
use App\Models\Event;
use App\Models\Instance;
use App\Models\Project;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class EventController extends Controller {
    public function handler(Request $request): Response {
        // validate the instance header
        $instance = $request->header('X-Gitlab-Instance') ?? throw new InvalidInstanceException();
        $instance = Instance::find($instance) ?? throw new UnrecognizedInstanceException($instance);

        // validate the token header
        $token = $request->header('X-Gitlab-Token');
        if ($token == '') throw new InvalidTokenException();

        // validate the content-type header
        if ($request->header('Content-Type') != 'application/json') return response(null, 415, ['Accept' => 'application/json']);

        // validate the request body
        $request->validate([
            'object_kind' => 'required|string|in:build,deployment,feature_flag,issue,merge_request,note,pipeline,push,release,tag_push,wiki_page',
            'project.id' => 'required|int',
        ]);

        $project = Project::where('instance_url', $instance->url)->where('gitlab_id', $request->input('project.id'))->first() ?? throw new UnrecognizedProjectException($instance->url, $request->input('project.id'));

        try {
            $event = Event::create([
                'uuid' => $request->header('X-Gitlab-Event-UUID'),
                'data' => $request->input(),
                'instance_url' => $instance->url,
                'project_id' => $project->id,
            ]);
        } catch (QueryException $e) {
            if (!str_starts_with($e->getMessage(), 'SQLSTATE[23000]: Integrity constraint violation: 19 UNIQUE constraint failed: events.uuid')) throw $e;
            throw new DuplicateEventUuidException();
        }

        ProcessEvent::dispatch($event);

        return response(null, 202); // accepted
    }
}
